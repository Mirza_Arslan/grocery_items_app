package com.grocery.items.deals;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/19/13
 * Time: 7:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class DailyDeal {

    private int dealId;
    private String dealName;
    private String dealItem;
    private int dealQty;
    private String dealMeasure;
    private String dealDate;

    /**
     *
     * @return
     */
    public int getDealId() {
        return dealId;
    }

    /**
     *
     * @param dealId
     */
    public void setDealId(int dealId) {
        this.dealId = dealId;
    }

    /**
     *
     * @return
     */
    public String getDealName() {
        return dealName;
    }

    /**
     *
     * @param dealName
     */
    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    /**
     *
     * @return
     */
    public String getDealItem() {
        return dealItem;
    }

    /**
     *
     * @param dealItem
     */
    public void setDealItem(String dealItem) {
        this.dealItem = dealItem;
    }

    /**
     *
     * @return
     */
    public int getDealQty() {
        return dealQty;
    }

    /**
     *
     * @param dealQty
     */
    public void setDealQty(int dealQty) {
        this.dealQty = dealQty;
    }

    /**
     *
     * @return
     */
    public String getDealMeasure() {
        return dealMeasure;
    }

    /**
     *
     * @param dealMeasure
     */
    public void setDealMeasure(String dealMeasure) {
        this.dealMeasure = dealMeasure;
    }

    /**
     *
     * @return
     */
    public String getDealDate() {
        return dealDate;
    }

    /**
     *
     * @param dealDate
     */
    public void setDealDate(String dealDate) {
        this.dealDate = dealDate;
    }
}
