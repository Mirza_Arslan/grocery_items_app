package com.grocery.items.templates;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.grocery.items.R;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;
import com.grocery.items.deals.DailyDeal;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/19/13
 * Time: 11:59 AM
 * To change this template use File | Settings | File Templates.
 */
public class SingleDealViewActivity extends Activity {

    private int selectedDealId;

    private TextView dealId;
    private TextView dealName;
    private TextView dealItemName;
    private TextView dealQtyAndMeasure;
    private TextView dealDate;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.single_deal_view_layout);

        setUpView();
    }

    private void setUpView() {
        Bundle extras = getIntent().getExtras();
        selectedDealId = extras.getInt("SELECTED_DEAL_ID");

        dealId = (TextView) findViewById(R.id.dealIdTextView);
        dealName = (TextView) findViewById(R.id.dealNameTextView);
        dealItemName = (TextView) findViewById(R.id.dealItemNameTextView);
        dealQtyAndMeasure = (TextView) findViewById(R.id.dealItemQuantityAndMeasureTextView);
        dealDate = (TextView) findViewById(R.id.dealDateTextView);

        DailyDeal getDeal = databaseSQLiteOpenHelper.getDealByDealId(selectedDealId);

        dealId.setText("Deal ID : " + getDeal.getDealId());
        dealName.setText("Deal Name : " + getDeal.getDealName());
        dealItemName.setText("Deal Item : " + getDeal.getDealItem());
        dealQtyAndMeasure.setText(getDeal.getDealQty() + " " + getDeal.getDealMeasure());
        dealDate.setText(getDeal.getDealDate());
    }
}
