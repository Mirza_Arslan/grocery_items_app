package com.grocery.items.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.grocery.items.R;
import com.grocery.items.Users.User;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Users: kavi
 * Date: 5/4/13
 * Time: 11:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class PasswordResetActivity extends Activity {

    private EditText currentPasswordEditText;
    private EditText newPasswordEditText;
    private EditText newPasswordConfirmEditText;

    private Button resetPasswordBackButton;
    private Button resetPasswordButton;

    private AlertDialog messageBalloonAlertDialog;
    final Context context = this;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password_reset_layout);

        setUpView();
    }

    private void setUpView() {
        currentPasswordEditText = (EditText) findViewById(R.id.currentPasswordEditText);
        newPasswordEditText = (EditText) findViewById(R.id.newPasswordEditText);
        newPasswordConfirmEditText = (EditText) findViewById(R.id.confirmPasswordEditText);

        resetPasswordBackButton = (Button) findViewById(R.id.resetPasswordBackButton);
        resetPasswordButton = (Button) findViewById(R.id.resetPasswordSaveButton);

        resetPasswordBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        resetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String currentPassword = currentPasswordEditText.getText().toString();
                String newPassword = newPasswordEditText.getText().toString();
                String newPasswordConfirm = newPasswordConfirmEditText.getText().toString();

                Map<String, String> lastLoginDetails = databaseSQLiteOpenHelper.getLoginDetails();
                if(lastLoginDetails.isEmpty()){
                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.errorLabel)
                            .setMessage("Valid user currently not sign in to the application")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    messageBalloonAlertDialog.cancel();
                                    finish();
                                }
                            }).create();
                    messageBalloonAlertDialog.show();
                } else {

                    String currentUserEmail = lastLoginDetails.get("email");
                    String currentUserPassword = lastLoginDetails.get("password");

                    if(!currentUserPassword.equals(currentPassword)) {
                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.errorLabel)
                                .setMessage("Current password is not match with the given password. Please check and re-enter")
                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        messageBalloonAlertDialog.cancel();
                                    }
                                }).create();
                        messageBalloonAlertDialog.show();
                    } else {
                        if(!newPassword.equals(newPasswordConfirm)){
                            messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                    .setTitle(R.string.errorLabel)
                                    .setMessage("New password confirmation is not match wth new password. Please check and re-enter")
                                    .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            messageBalloonAlertDialog.cancel();
                                        }
                                    }).create();
                            messageBalloonAlertDialog.show();
                        } else {
                            ArrayList<User> getUserList = databaseSQLiteOpenHelper.getUserFromEmail(currentUserEmail);
                            if(getUserList.isEmpty()) {
                                messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                        .setTitle(R.string.errorLabel)
                                        .setMessage("Error occurred while updating the user password. No user available from " + currentUserEmail)
                                        .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                messageBalloonAlertDialog.cancel();
                                                finish();
                                            }
                                        }).create();
                                messageBalloonAlertDialog.show();
                            } else {
                                if(getUserList.size() != 1) {
                                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                            .setTitle(R.string.errorLabel)
                                            .setMessage("Invalid user selection from " + currentUserEmail + " email")
                                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    messageBalloonAlertDialog.cancel();
                                                    finish();
                                                }
                                            }).create();
                                    messageBalloonAlertDialog.show();
                                } else {
                                    User selectUser = getUserList.get(0);
                                    selectUser.setPassword(newPassword);
                                    boolean result = databaseSQLiteOpenHelper.updateUser(selectUser);
                                    if(result) {
                                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                                .setTitle(R.string.successLabel)
                                                .setMessage("Successfully updated the password")
                                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        messageBalloonAlertDialog.cancel();
                                                        finish();
                                                    }
                                                }).create();
                                        messageBalloonAlertDialog.show();
                                    } else {
                                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                                .setTitle(R.string.successLabel)
                                                .setMessage("Error occurred while updating the user password.")
                                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        messageBalloonAlertDialog.cancel();
                                                        finish();
                                                    }
                                                }).create();
                                        messageBalloonAlertDialog.show();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }
}
