package com.grocery.items.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.grocery.items.Items.Item;
import com.grocery.items.R;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/14/13
 * Time: 11:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddNewItemActivity extends Activity {

    private EditText itemNameEditText;
    private EditText itemQuantityEditText;
    private Spinner itemMeasureSpinner;
    private Button addItemButton;
    private Button resetViewButton;
    private Button backButton;

    private AlertDialog messageBalloonAlertDialog;
    final Context context = this;

    private ArrayList<Item> selectedItemList;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.add_new_item_layout);

        setUpView();
    }

    private void setUpView() {

        itemNameEditText = (EditText) findViewById(R.id.itemNameEditText);
        itemQuantityEditText = (EditText) findViewById(R.id.itemQuantityEditText);
        itemMeasureSpinner = (Spinner) findViewById(R.id.measureSpinner);
        addItemButton = (Button) findViewById(R.id.addNewItemButton);
        resetViewButton = (Button) findViewById(R.id.resetAddItemButton);
        backButton = (Button) findViewById(R.id.backItemListButton);

        addItemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int existItemId = -1;

                String itemName = itemNameEditText.getText().toString();
                String itemQuantity = itemQuantityEditText.getText().toString();
                String itemMeasure = itemMeasureSpinner.getSelectedItem().toString();

                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Calendar cal = Calendar.getInstance();
                final String currentDateTime = dateFormat.format(cal.getTime());

                selectedItemList = databaseSQLiteOpenHelper.getAllItems();

                final Item insertItem = new Item();
                for (Item item : selectedItemList) {
                    if(item.getItemName().equals(itemName)){
                        existItemId = item.getItemId();
                        insertItem.setItemId(existItemId);
                    }
                }


                insertItem.setItemName(itemName);
                insertItem.setQuantity(Integer.parseInt(itemQuantity));
                insertItem.setMeasure(itemMeasure);

                if(existItemId != -1) {
                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.warningLabel)
                            .setMessage("Item is already exists from this item name. Do you need to update exiting or change item name")
                            .setPositiveButton("update", new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //need to write item update function and call
                                    insertItem.setDateUpdated(currentDateTime);
                                    boolean updateResult = databaseSQLiteOpenHelper.updateItem(insertItem);
                                    if (updateResult){
                                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                                .setTitle(R.string.successLabel)
                                                .setMessage("Successfully updated the selected Item")
                                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        messageBalloonAlertDialog.cancel();
                                                    }
                                                }).create();
                                        messageBalloonAlertDialog.show();
                                    } else {

                                    }
                                }
                            })
                            .setNeutralButton(R.string.cancel, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    itemNameEditText.setText("");
                                    messageBalloonAlertDialog.cancel();
                                }
                            }).create();
                    messageBalloonAlertDialog.show();
                } else {
                    insertItem.setDateEntered(currentDateTime);
                    insertItem.setDateUpdated(" --- ");
                    boolean insertResult = databaseSQLiteOpenHelper.addNewItem(insertItem);
                    if(insertResult) {
                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.successLabel)
                                .setMessage("Item inserted successfully")
                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        messageBalloonAlertDialog.cancel();
                                    }
                                }).create();
                        messageBalloonAlertDialog.show();
                    }
                }
            }
        });

        resetViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemNameEditText.setText("");
                itemQuantityEditText.setText("");
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itemListViewIntent = new Intent(AddNewItemActivity.this, ItemsViewActivity.class);
                finish();
                startActivity(itemListViewIntent);
            }
        });
    }
}
