package com.grocery.items.templates;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import com.grocery.items.LoginActivity;
import com.grocery.items.R;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;

/**
 * Created with IntelliJ IDEA.
 * Users: kavi
 * Date: 5/5/13
 * Time: 8:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class MainMenuActivity extends Activity {

    private Button itemsButton;
    private Button searchStoreButton;
    private Button dailyCouponsButton;
    private Button settingsButton;
    private Button signOutButton;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.main_menu_layout);

        setUpViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.changeEmailMenuItem:
                Intent resetEmailIntent = new Intent(MainMenuActivity.this, EmailResetActivity.class);
                startActivity(resetEmailIntent);
                return true;
            case R.id.changePasswordMenuItem:
                Intent passwordResetIntent = new Intent(MainMenuActivity.this, PasswordResetActivity.class);
                startActivity(passwordResetIntent);
                return true;
            case R.id.about:
                return true;
            case R.id.exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUpViews() {

        itemsButton = (Button) findViewById(R.id.itemsButton);
        searchStoreButton = (Button) findViewById(R.id.searchStoresButton);
        dailyCouponsButton = (Button) findViewById(R.id.dailyCouponsButton);
        settingsButton = (Button) findViewById(R.id.settingsButton);
        signOutButton = (Button) findViewById(R.id.signOutButton);

        signOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                databaseSQLiteOpenHelper.deleteLoginDetails();
                Intent loginActivityIntent = new Intent(MainMenuActivity.this, LoginActivity.class);
                startActivity(loginActivityIntent);
                finish();
            }
        });

        itemsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itemListActivityIntent = new Intent(MainMenuActivity.this, ItemsViewActivity.class);
                startActivity(itemListActivityIntent);
            }
        });

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent settingActivityIntent = new Intent(MainMenuActivity.this, SettingsActivity.class);
                startActivity(settingActivityIntent);
            }
        });

        dailyCouponsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dailyDealsActivityIntent = new Intent(MainMenuActivity.this, DealsViewActivity.class);
                startActivity(dailyDealsActivityIntent);
            }
        });

        searchStoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent searchStoresActivityIntent = new Intent(MainMenuActivity.this, SearchStoresActivity.class);
                startActivity(searchStoresActivityIntent);
            }
        });
    }
}
