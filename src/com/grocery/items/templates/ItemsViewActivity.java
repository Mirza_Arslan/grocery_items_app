package com.grocery.items.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.grocery.items.Items.Item;
import com.grocery.items.R;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/13/13
 * Time: 11:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class ItemsViewActivity extends Activity {

    private ListView currentItemListView;

    private ArrayList<Item> currentItemList = new ArrayList<Item>();
    private ArrayList<String> itemNameList = new ArrayList<String>();
    private ArrayAdapter<String> itemListAdapter;

    private AlertDialog messageBalloonAlertDialog;
    final Context context = this;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.items_view_layout);

        setUpViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_item_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.addItemMenuItem:
                Intent addItemIntent = new Intent(ItemsViewActivity.this, AddNewItemActivity.class);
                finish();
                startActivity(addItemIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUpViews() {
        currentItemListView = (ListView) findViewById(R.id.itemListView);

        currentItemList = databaseSQLiteOpenHelper.getAllItems();

        if(currentItemList.isEmpty()) {
            messageBalloonAlertDialog = new AlertDialog.Builder(context)
                    .setTitle(R.string.warningLabel)
                    .setMessage("No records for View")
                    .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            messageBalloonAlertDialog.cancel();
                        }
                    }).create();
            messageBalloonAlertDialog.show();
        } else {
            for(Item item : currentItemList) {
                itemNameList.add("   " + item.getItemId() + " - " + item.getItemName());
            }

            itemListAdapter = new ArrayAdapter<String>(this, R.layout.item_view_row, itemNameList);
            currentItemListView.setAdapter(itemListAdapter);
        }

        currentItemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String itemContent = (String) (currentItemListView.getItemAtPosition(i));
                String[] content = itemContent.split("-");
                String id = content[0].trim();
                Intent singleItemIntent = new Intent(ItemsViewActivity.this, SingleItemViewActivity.class);
                singleItemIntent.putExtra("SELECTED_ITEM_ID", Integer.parseInt(id));
                startActivity(singleItemIntent);
            }
        });
    }
}
