package com.grocery.items.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import com.grocery.items.R;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;
import com.grocery.items.deals.DailyDeal;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/19/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class AddNewDealActivity extends Activity {

    private EditText dealNameEditText;
    private EditText dealItemNameEditText;
    private EditText dealQtyEditText;
    private Spinner dealMeasureSpinner;

    private Button saveDealButton;
    private Button resetDealButton;
    private Button backButton;

    private AlertDialog messageBalloonAlertDialog;
    final Context context = this;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.add_new_deal_layout);

        setUpView();
    }

    private void setUpView() {

        dealNameEditText = (EditText) findViewById(R.id.dealNameEditText);
        dealItemNameEditText = (EditText) findViewById(R.id.dealItemNameEditText);
        dealQtyEditText = (EditText) findViewById(R.id.dealItemQuantityEditText);
        dealMeasureSpinner = (Spinner) findViewById(R.id.dealMeasureSpinner);

        saveDealButton = (Button) findViewById(R.id.addNewDealButton);
        resetDealButton = (Button) findViewById(R.id.resetAddDealButton);
        backButton = (Button) findViewById(R.id.backDealListButton);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dealsViewActivityIntent = new Intent(AddNewDealActivity.this, DealsViewActivity.class);
                startActivity(dealsViewActivityIntent);
                finish();
            }
        });

        resetDealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dealNameEditText.setText("");
                dealItemNameEditText.setText("");
                dealQtyEditText.setText("");
            }
        });

        saveDealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dealName = dealNameEditText.getText().toString();
                String dealItemName = dealItemNameEditText.getText().toString();
                String dealQty = dealQtyEditText.getText().toString();
                String dealMeasure = dealMeasureSpinner.getSelectedItem().toString();

                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Calendar cal = Calendar.getInstance();
                String currentDateTime = dateFormat.format(cal.getTime());

                DailyDeal deal = new DailyDeal();
                deal.setDealName(dealName);
                deal.setDealItem(dealItemName);
                deal.setDealQty(Integer.parseInt(dealQty));
                deal.setDealMeasure(dealMeasure);
                deal.setDealDate(currentDateTime);

                boolean result = databaseSQLiteOpenHelper.addNewDeal(deal);
                if (result) {
                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.successLabel)
                            .setMessage("Deal inserted successfully")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    messageBalloonAlertDialog.cancel();
                                    dealNameEditText.setText("");
                                    dealItemNameEditText.setText("");
                                    dealQtyEditText.setText("");
                                }
                            }).create();
                    messageBalloonAlertDialog.show();
                } else {
                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.errorLabel)
                            .setMessage("Error occurred while inserting new deal to the database")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    messageBalloonAlertDialog.cancel();
                                }
                            }).create();
                    messageBalloonAlertDialog.show();
                }
            }
        });
    }
}
