package com.grocery.items.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.grocery.items.R;
import com.grocery.items.Users.User;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/18/13
 * Time: 11:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class EmailResetActivity extends Activity {

    private EditText currentEmailEditText;
    private EditText newEmailEditText;
    private Button changeEmailSaveButton;
    private Button changeEmailBackButton;

    private AlertDialog messageBalloonAlertDialog;
    final Context context = this;

    private Map<String, String> currentLoginDetails;
    private ArrayList<User> userList;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.email_reset_layout);

        setUpViews();
    }

    private void setUpViews() {

        currentEmailEditText = (EditText) findViewById(R.id.currentEmailEditText);
        newEmailEditText = (EditText) findViewById(R.id.newEmailEditText);
        changeEmailSaveButton = (Button) findViewById(R.id.resetEmailSaveButton);
        changeEmailBackButton = (Button) findViewById(R.id.resetEmailBackButton);

        changeEmailSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String currentEmail = currentEmailEditText.getText().toString();
                String newEmail = newEmailEditText.getText().toString();

                currentLoginDetails = databaseSQLiteOpenHelper.getLoginDetails();

                if(!currentEmail.equals(currentLoginDetails.get("email"))) {
                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.errorLabel)
                            .setMessage("Current email is not match with the given email")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    messageBalloonAlertDialog.cancel();
                                }
                            }).create();
                    messageBalloonAlertDialog.show();
                } else {
                    userList = databaseSQLiteOpenHelper.getUserFromEmail(currentEmail);
                    if (userList.isEmpty()) {
                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.errorLabel)
                                .setMessage("User is not found from the given email")
                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        messageBalloonAlertDialog.cancel();
                                    }
                                }).create();
                        messageBalloonAlertDialog.show();
                    } else {
                        if(userList.size() == 1) {
                            User selectUser = userList.get(0);
                            selectUser.setEmail(newEmail);

                            Map<String, String> newLoginDetails = new HashMap<String, String>();
                            newLoginDetails.put("stayLoginStatus", currentLoginDetails.get("stayLoginStatus"));
                            newLoginDetails.put("logoutStatus", currentLoginDetails.get("logoutStatus"));
                            newLoginDetails.put("email", newEmail);
                            newLoginDetails.put("password", currentLoginDetails.get("password"));

                            //update user table
                            boolean UserResult = databaseSQLiteOpenHelper.updateUser(selectUser);
                            //update last login detail table
                            databaseSQLiteOpenHelper.deleteLoginDetails();
                            boolean loginDetailResults = databaseSQLiteOpenHelper.insertLoginDetails(newLoginDetails);

                            if(UserResult && loginDetailResults) {
                                messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                        .setTitle(R.string.successLabel)
                                        .setMessage("Successfully updated the email")
                                        .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                messageBalloonAlertDialog.cancel();
                                                finish();
                                            }
                                        }).create();
                                messageBalloonAlertDialog.show();
                            } else {
                                messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                        .setTitle(R.string.errorLabel)
                                        .setMessage("Error occurred while updating the email")
                                        .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                messageBalloonAlertDialog.cancel();
                                            }
                                        }).create();
                                messageBalloonAlertDialog.show();
                            }
                        }
                    }
                }
            }
        });

        changeEmailBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
