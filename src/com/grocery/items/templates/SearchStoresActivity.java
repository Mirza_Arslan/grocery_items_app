package com.grocery.items.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.grocery.items.R;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/19/13
 * Time: 1:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class SearchStoresActivity extends Activity {

    private EditText zipCodeEditText;
    private Button searchButton;

    private AlertDialog messageBalloonAlertDialog;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.search_stores_layout);

        setUpView();
    }

    private void setUpView() {
        zipCodeEditText = (EditText) findViewById(R.id.zipCodeEditText);
        searchButton = (Button) findViewById(R.id.searchStoresButton);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String zipCode = zipCodeEditText.getText().toString();

                messageBalloonAlertDialog = new AlertDialog.Builder(context)
                        .setTitle(R.string.warningLabel)
                        .setMessage("Sorry, No data source for searching stores from zip code")
                        .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                messageBalloonAlertDialog.cancel();
                                finish();
                            }
                        }).create();
                messageBalloonAlertDialog.show();
            }
        });
    }
}
