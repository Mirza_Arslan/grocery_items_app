package com.grocery.items.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.grocery.items.R;
import com.grocery.items.Users.User;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/11/13
 * Time: 6:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class ForgotPasswordResetActivity extends Activity {

    private EditText yourEmailEditText;
    private Button emailCheckButton;
    private EditText newPasswordEditText;
    private EditText confirmPasswordEditText;
    private Button resetPasswordButton;

    private AlertDialog messageBalloonAlertDialog;
    final Context context = this;

    private ArrayList<User> user;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.forgot_password_reset_layout);

        setUpViews();
    }

    private void setUpViews() {

        yourEmailEditText = (EditText) findViewById(R.id.youEmailEditText);
        emailCheckButton = (Button) findViewById(R.id.checkEmailButton);
        newPasswordEditText = (EditText) findViewById(R.id.newForgotPasswordEditText);
        confirmPasswordEditText = (EditText) findViewById(R.id.confirmForgotPasswordEditText);
        resetPasswordButton = (Button) findViewById(R.id.forgotPasswordResetButton);

        newPasswordEditText.setEnabled(false);
        confirmPasswordEditText.setEnabled(false);
        resetPasswordButton.setEnabled(false);

        emailCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String checkEmail = yourEmailEditText.getText().toString();
                user = databaseSQLiteOpenHelper.getUserFromEmail(checkEmail);

                if (user.isEmpty()) {
                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.warningLabel)
                            .setMessage("No user register from the given email")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    messageBalloonAlertDialog.cancel();
                                }
                            }).create();
                    messageBalloonAlertDialog.show();
                } else {
                    if (user.size() != 1) {
                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.warningLabel)
                                .setMessage("Invalid user select from given email")
                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        messageBalloonAlertDialog.cancel();
                                    }
                                }).create();
                        messageBalloonAlertDialog.show();
                    } else {
                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.successLabel)
                                .setMessage("User found. Please give new passwords to change")
                                .setPositiveButton(R.string.ok, new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        newPasswordEditText.setEnabled(true);
                                        confirmPasswordEditText.setEnabled(true);
                                        resetPasswordButton.setEnabled(true);
                                    }
                                }).create();
                        messageBalloonAlertDialog.show();
                    }
                }

            }
        });

        resetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String newPassword = newPasswordEditText.getText().toString();
                String confirmPassword = confirmPasswordEditText.getText().toString();

                if(!newPassword.equals(confirmPassword)) {
                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.errorLabel)
                            .setMessage("Password confirmation is not match")
                            .setPositiveButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    yourEmailEditText.setText("");
                                    messageBalloonAlertDialog.cancel();
                                }
                            }).create();
                    messageBalloonAlertDialog.show();
                } else {
                    User getUser = user.get(0);
                    getUser.setPassword(newPassword);
                    boolean result = databaseSQLiteOpenHelper.updateUser(getUser);
                    if(result) {
                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.successLabel)
                                .setMessage("Successfully updated your password")
                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        newPasswordEditText.setEnabled(false);
                                        confirmPasswordEditText.setEnabled(false);
                                        resetPasswordButton.setEnabled(false);
                                        yourEmailEditText.setText("");
                                        messageBalloonAlertDialog.cancel();
                                    }
                                }).create();
                        messageBalloonAlertDialog.show();
                    } else {
                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.errorLabel)
                                .setMessage("Error occurred while updating your password. Please try again later")
                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        newPasswordEditText.setEnabled(false);
                                        confirmPasswordEditText.setEnabled(false);
                                        resetPasswordButton.setEnabled(false);

                                        newPasswordEditText.setText("");
                                        confirmPasswordEditText.setText("");
                                        messageBalloonAlertDialog.cancel();
                                    }
                                }).create();
                        messageBalloonAlertDialog.show();
                    }
                }
            }
        });
    }
}
