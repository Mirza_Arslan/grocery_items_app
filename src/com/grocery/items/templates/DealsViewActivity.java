package com.grocery.items.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.grocery.items.R;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;
import com.grocery.items.deals.DailyDeal;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: kavi
 * Date: 5/19/13
 * Time: 8:01 AM
 * To change this template use File | Settings | File Templates.
 */
public class DealsViewActivity extends Activity {

    private ListView currentDealListView;

    private AlertDialog messageBalloonAlertDialog;
    final Context context = this;

    private ArrayList<DailyDeal> currentDealList = new ArrayList<DailyDeal>();
    private ArrayList<String> dealNameList = new ArrayList<String>();
    private ArrayAdapter<String> dealListAdapter;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.deals_view_layout);

        setUpView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_deal_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.addDealMenuItem:
                Intent addNewDealActivityIntent = new Intent(DealsViewActivity.this, AddNewDealActivity.class);
                startActivity(addNewDealActivityIntent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setUpView() {
        currentDealListView = (ListView) findViewById(R.id.dealListView);

        currentDealList = databaseSQLiteOpenHelper.getAllDeals();

        if(currentDealList.isEmpty()) {
            messageBalloonAlertDialog = new AlertDialog.Builder(context)
                    .setTitle(R.string.warningLabel)
                    .setMessage("No records for View")
                    .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            messageBalloonAlertDialog.cancel();
                        }
                    }).create();
            messageBalloonAlertDialog.show();
        } else {
            for(DailyDeal deal : currentDealList) {
                dealNameList.add("  " + deal.getDealId() + " - " + deal.getDealName() + " - Qty: " + deal.getDealQty() + " " + deal.getDealMeasure());
            }

            dealListAdapter = new ArrayAdapter<String>(this, R.layout.deal_view_row, dealNameList);
            currentDealListView.setAdapter(dealListAdapter);
        }

        currentDealListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String itemContent = (String) (currentDealListView.getItemAtPosition(i));
                String[] content = itemContent.split("-");
                String id = content[0].trim();
                Intent singleDealIntent = new Intent(DealsViewActivity.this, SingleDealViewActivity.class);
                singleDealIntent.putExtra("SELECTED_DEAL_ID", Integer.parseInt(id));
                startActivity(singleDealIntent);
            }
        });
    }
}
