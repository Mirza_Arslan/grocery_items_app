package com.grocery.items.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.grocery.items.R;
import com.grocery.items.Users.User;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * Users: kavi
 * Date: 5/5/13
 * Time: 7:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class SignUpNewUserActivity extends Activity {

    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText confirmPasswordEditText;

    private Button signUpBackButton;
    private Button signUpButton;

    private AlertDialog messageBalloonAlertDialog;
    final Context context = this;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.signup_layout);

        setUpViews();
    }

    private void setUpViews() {

        firstNameEditText = (EditText) findViewById(R.id.firstNameEditText);
        firstNameEditText.setText("");

        lastNameEditText = (EditText) findViewById(R.id.lastNameEditText);
        lastNameEditText.setText("");

        emailEditText = (EditText) findViewById(R.id.emailEditText);
        emailEditText.setText("");

        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        passwordEditText.setText("");

        confirmPasswordEditText = (EditText) findViewById(R.id.passwordConfirmEditText);
        confirmPasswordEditText.setText("");

        signUpButton = (Button) findViewById(R.id.signUpButton);
        signUpBackButton = (Button) findViewById(R.id.signUpBackButton);

        signUpBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean result = false;

                String firstName = firstNameEditText.getText().toString();
                Log.d("fname", firstName);
                String lastName = lastNameEditText.getText().toString();
                Log.d("lname", lastName);
                String email = emailEditText.getText().toString();
                Log.d("email", email);
                String password = passwordEditText.getText().toString();
                Log.d("pass", password);
                String confirmPassword = confirmPasswordEditText.getText().toString();
                Log.d("conf", confirmPassword);

                if(email == null || email.equals("") || password == null || password.equals("") || confirmPassword == null || confirmPassword.equals("")) {
                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.errorLabel)
                            .setMessage("Please fill the email and password. Those are required fields")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    messageBalloonAlertDialog.cancel();
                                }
                            }).create();
                    messageBalloonAlertDialog.show();
                } else {
                    if (!password.equals(confirmPassword)) {
                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.errorLabel)
                                .setMessage("Password confirmation is not match. Please check the password and re-enter")
                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        messageBalloonAlertDialog.cancel();
                                        passwordEditText.setText("");
                                        confirmPasswordEditText.setText("");
                                    }
                                }).create();
                        messageBalloonAlertDialog.show();
                    } else {
                        //create user object and initialized the object
                        User user = new User();
                        user.setFirstName(firstName);
                        user.setLastName(lastName);
                        user.setEmail(email);
                        user.setPassword(password);

                        try {
                            ArrayList<User> getUser = databaseSQLiteOpenHelper.getUserFromEmail(user.getEmail());

                            if(!getUser.isEmpty()) {
                                messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                        .setTitle(R.string.errorLabel)
                                        .setMessage("Give email is already exists")
                                        .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                messageBalloonAlertDialog.cancel();
                                            }
                                        }).create();
                                messageBalloonAlertDialog.show();
                            } else {
                                result = databaseSQLiteOpenHelper.addNewUser(user);
                                if (result) {
                                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                        .setTitle(R.string.successLabel)
                                        .setMessage("Successfully created the user for " + user.getEmail() + " Email")
                                        .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                finish();
                                            }
                                        }).create();
                                    messageBalloonAlertDialog.show();
                                } else {
                                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                        .setTitle(R.string.errorLabel)
                                        .setMessage("Error occurred while creating the user for " + user.getEmail() + " Email")
                                        .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                finish();
                                            }
                                        }).create();
                                    messageBalloonAlertDialog.show();
                                }
                            }
                        } catch (Exception e) {
                            messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                    .setTitle(R.string.errorLabel)
                                    .setMessage("Error occurred while creating the user for " + user.getEmail() + " Email")
                                    .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            finish();
                                        }
                                    }).create();
                            messageBalloonAlertDialog.show();
                        }
                    }
                }
            }
        });
    }
}
