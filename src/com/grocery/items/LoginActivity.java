package com.grocery.items;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.grocery.items.Users.User;
import com.grocery.items.database.DatabaseSQLiteOpenHelper;
import com.grocery.items.templates.ForgotPasswordResetActivity;
import com.grocery.items.templates.MainMenuActivity;
import com.grocery.items.templates.SignUpNewUserActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    private EditText loginEmailEditText;
    private EditText loginPasswordEditText;
    private CheckBox stayLoginCheckBox;
    private TextView forgotPasswordTextView;
    private Button signInButton;
    private Button signUpButton;

    private AlertDialog messageBalloonAlertDialog;
    final Context context = this;

    private DatabaseSQLiteOpenHelper databaseSQLiteOpenHelper = new DatabaseSQLiteOpenHelper(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Map<String, String> lastLoginDetails = databaseSQLiteOpenHelper.getLoginDetails();
        if(!lastLoginDetails.isEmpty()) {
            Log.d("email", lastLoginDetails.get("email"));
            Log.d("stayLoginStatus", lastLoginDetails.get("stayLoginStatus"));
            Log.d("logoutStatus", lastLoginDetails.get("logoutStatus"));

            if(lastLoginDetails.get("stayLoginStatus").equals("1")){
                Intent mainMenuLayoutIntent = new Intent(LoginActivity.this, MainMenuActivity.class);
                startActivity(mainMenuLayoutIntent);
                finish();
            }
        }

        setUpView();
    }

    private void setUpView() {

        loginEmailEditText = (EditText) findViewById(R.id.emailEditText);
        loginPasswordEditText = (EditText) findViewById(R.id.passwordEditText);
        stayLoginCheckBox = (CheckBox) findViewById(R.id.stayLoginCheckBox);
        forgotPasswordTextView = (TextView) findViewById(R.id.forgotPasswordLink);

        signInButton = (Button) findViewById(R.id.loginButton);
        signUpButton = (Button) findViewById(R.id.registerButton);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent signUpLayoutIntent = new Intent(LoginActivity.this, SignUpNewUserActivity.class);
                startActivity(signUpLayoutIntent);
            }
        });

        forgotPasswordTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent forgotPasswordLayoutIntent = new Intent(LoginActivity.this, ForgotPasswordResetActivity.class);
                startActivity(forgotPasswordLayoutIntent);
            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = loginEmailEditText.getText().toString().trim();
                String password = loginPasswordEditText.getText().toString().trim();

                ArrayList<User> getUser = databaseSQLiteOpenHelper.getUserFromEmail(email);
                if(getUser.isEmpty()){
                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.errorLabel)
                            .setMessage("User doesn't exists")
                            .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    messageBalloonAlertDialog.cancel();
                                }
                            }).create();
                    messageBalloonAlertDialog.show();
                } else {
                    if(getUser.size() != 1){
                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.errorLabel)
                                .setMessage("Invalid user email. There are several users from this email")
                                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        messageBalloonAlertDialog.cancel();
                                    }
                                }).create();
                        messageBalloonAlertDialog.show();
                    } else {
                        for (User user : getUser) {
                            if (user.getPassword().equals(password)) {
                                loginEmailEditText.setText("");
                                loginPasswordEditText.setText("");

                                databaseSQLiteOpenHelper.deleteLoginDetails();

                                Map data = new HashMap<String, String>();

                                if(stayLoginCheckBox.isChecked()){

                                    data.put("stayLoginStatus", "1");
                                    data.put("logoutStatus", "0");
                                    data.put("email", email);
                                    data.put("password", password);

                                    databaseSQLiteOpenHelper.insertLoginDetails(data);
                                } else {
                                    data.put("stayLoginStatus", "0");
                                    data.put("logoutStatus", "0");
                                    data.put("email", email);
                                    data.put("password", password);

                                    databaseSQLiteOpenHelper.insertLoginDetails(data);
                                }
                                Intent mainMenuLayoutIntent = new Intent(LoginActivity.this, MainMenuActivity.class);
                                startActivity(mainMenuLayoutIntent);
                                finish();
                            } else {
                                messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                        .setTitle(R.string.errorLabel)
                                        .setMessage("Authentication failure. Please check your email and password")
                                        .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                loginPasswordEditText.setText("");
                                                messageBalloonAlertDialog.cancel();
                                            }
                                        }).create();
                                messageBalloonAlertDialog.show();
                            }
                        }
                    }
                }

//                Intent mainMenuLayoutIntent = new Intent(LoginActivity.this, MainMenuActivity.class);
//                startActivity(mainMenuLayoutIntent);
            }
        });
    }
}
